const express = require('express');
const router = express.Router();
const request = require('request-promise');
const knex = require('../static_data/db');
const _ = require('lodash');
const forceUpdate = require('../controller/update.js');

const respondWithError = function(res,err) {
  console.log(err);
  return res.send(err);
}

router.get('/',(req,res) => {
  forceUpdate(res);
});

router.get('/:artist',(req,res) => {
  let options = {};
  options.uri = `https://rest.bandsintown.com/artists/${req.params.artist}/events?app_id=sda`,
  request(options)
    .then(response => {
      console.log(response);
      // need to delete a plenty of information away
      res.setHeader('Content-Type', 'application/json');
      let resp = JSON.parse(response);
      let cutresp = _.map(resp, (value) => {
        // delete value.id;
        return value;
      });
      res.send(resp);
    })
    .catch(e=>respondWithError(res,e));
});


module.exports = router;
