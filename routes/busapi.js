const express = require('express');
const router = express.Router();
const request = require('request-promise');
const options = require('../static_data/options');

const respondWithError = function(res,err) {
  console.log(err);
  return res.send(err);
}

/* GET test response from bus-api */
router.get('/test', function(req, res, next) {
request(options)
  .then(response => {
    console.log(response);
    res.send(response);
  })
  .catch(e=>respondWithError(res,e));
});

module.exports = router;
