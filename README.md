== Setup Instructions ==

cd to this dir and run in your term:

```
    npm install
```

Replace the 

```
    "scripts": {
    	"start": "set prefix=misha_&&node ./bin/www"
  	},
```

with 

```
    "scripts": {
    	"start": "prefix=UOURPREFIX_ node ./bin/www"
  	},
````

Run in your term

```
	npm start
```