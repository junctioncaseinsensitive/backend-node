

var conf = {
  client: 'mysql',
  connection: {
    host : 'junctioncaseinsensitive.cd3fgxfmta9j.eu-west-1.rds.amazonaws.com',
    user : 'b5de93b1f48eb2',
    password : 'f025fe63',
    database : 'junctioncaseinsensitive'
  },
  pool: { min: 0, max: 5 }
}

function handleKnexDisconnect(conf) {
  var knex = require('knex')(conf); // Recreate the connection, since
                                                  // the old one cannot be reused.

  // connection.getConnection(function(err) {              // The server is either down
  //   if(err) {                                     // or restarting (takes a while sometimes).
  //     console.log('error when connecting to db:', err);
  //     setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
  //   }                                     // to avoid a hot loop, and to allow our node script to
  // });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  knex.on('disconnect', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      console.log('error when connecting to db:', err);
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      // throw err;
      handleDisconnect();                               // server variable configures this)
    }
  });
  return knex
}
var knex = handleKnexDisconnect(conf)

module.exports = knex
