const request = require('request-promise');
const knex = require('../static_data/db');
const _ = require('lodash');
const Promise = require('bluebird')
const obj = {
  status:'updated',
  timestamp:Math.floor(Date.now())
}

var CronJob = require('cron').CronJob;
new CronJob('*/1 * * * *', function() {
  console.log('starting crone auoto update stuff');
  forceUpdate();
}, null, true, 'America/Los_Angeles');

const PromiseBasedArtistWrapper = function(artist_trip_row,trip_id) {
  return new Promise((resolve,reject)=>{
      let a = new FillTheConcertTable(artist_trip_row.artist_id);
      a.artist_trip_process_id = artist_trip_row.trip_process_id;
      console.log(a.artist_trip_process_id);
      a.getArtistInfo()
      .then(rows=>{
        if(rows.length!==0){
          a.artist_id = rows[0].id;
          a.artist_name = rows[0].name;
          a.getBandsintownApiRequest()
          .then(result => {
            const parsed = JSON.parse(result);
            knex
            .select()
            .from(`${process.env.prefix}events`)
            .where('artist_id', a.artist_id)
            .then((ev) => {
              if(ev.length!==0){
                a.updateTripProcesses(trip_id,resolve);
                // resolve()
              } else {
                let promiseArrayToInsert = [];
                for(let i in parsed) {
                  promiseArrayToInsert.push(a.insertVenue(parsed[i]));
                }
                Promise.all(promiseArrayToInsert)
                .then(result=>{
                  // a.updateTripProcesses(trip_id,forceUpdate,res);
                  a.updateTripProcesses(trip_id,resolve)
                })
              }
            })
            .catch(e=>console.log(e))
          })
          .catch(e=>{
            console.log(e);
            a.updateTripProcesses(trip_id,resolve);
          })
        } else {
          console.log('no artists found');
          res.send(obj)
        }
      })
      .catch(e=>console.log(e))
  });
}

const forceUpdate = function(res) {
  if(typeof res==='undefined') {
    var res = {};
    res.send = function(param) {
      console.log(param);
    }
  }
  knex
  .select()
  .from(`${process.env.prefix}trip_processes`)
  .where('artists',1)
  .andWhere('events',0)
  .then(rows => {
    if(rows.length===0){
      res.send(obj);
    } else {
      let trip_id = rows[0].id;
      knex
      .select()
      .from(`${process.env.prefix}artist_trip_process`)
      .where('trip_process_id',trip_id)
      .then(artist_trip_rows=>{
        if(artist_trip_rows.length!==0){
          let arrayToProceed = [];
          for(let i in artist_trip_rows) {
            arrayToProceed.push(PromiseBasedArtistWrapper(artist_trip_rows[i],trip_id));
          }
          Promise.all(arrayToProceed)
          .then(()=>{
            forceUpdate(res)
          })
        } else {
          res.send(obj);
        }
      })
    }
  })
  .catch(e=>{
    console.log(e);
    res.send(e);
  })
}


class FillTheConcertTable {
  constructor(artistId) {
    this.artist_id = artistId;
  }
  getBandsintownApiRequest(artist) {
      let options = {};
      options.uri = `https://rest.bandsintown.com/artists/${this.artist_name}/events?app_id=sda`;
      return request(options)
  }
  getArtistInfo() {
    return knex
    .select()
    .from(`${process.env.prefix}artists`)
    .where('id',this.artist_id)
  }
  updateTripProcesses(id,cb) {
    return knex(`${process.env.prefix}trip_processes`)
    .update({events:1})
    .where('id',id)
    .then(()=>{
      cb();
    })
    .catch(e=>console.log(e));
  }
  insertVenue(ev) {
    console.log(knex.fn.now());
    let dat = new Date().toString();
    return knex(`${process.env.prefix}events`)
    .insert({
      artist_id: this.artist_id,
      lat: ev.venue.latitude,
      process_id: this.artist_trip_process_id,
      long: ev.venue.longitude,
      city_name: ev.venue.city,
      date: ev.datetime,
      name: ev.venue.name,
      created_at: knex.fn.now(),
      updated_at: knex.fn.now()
    })
  }
}


console.log('update mounted');
module.exports = forceUpdate;
